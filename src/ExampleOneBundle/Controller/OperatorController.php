<?php

namespace ExampleOneBundle\Controller;

use ExampleOneBundle\Entity\Item;
use ExampleOneBundle\Entity\Operator;
use ExampleOneBundle\Service\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OperatorController extends Controller {

    //removed unnecessary properties
    //made some functions private because there is no point to keep them protected
    public function indexAction() {
        
        //init empty cart object 
        
        $cart = new Cart();

        //getting prepared items to insert into cart. As you said its not typical situation

        $item1 = $this->getCartItem1();
        $item2 = $this->getCartItem2();

        //adding items to cart
        
        $cart->addItem($item1);
        $cart->addItem($item2);
        
        //getting cart total value using service method directly assigning it to variable because it might be used below
        
        $oldTotal = $cart->getTotal();

        //prevented unnecesary statements on front-end side of applicatioon
        
        $templateVars = array(
            'oldTotal' => $oldTotal
        );

        //getting parameter if operator or null if not
        
        $operatorPriceModifier = $this->getOperatoroPriceModifier();

        //made the statement shorter in result coding is faster and self-explain:) 
        
        if ($operatorPriceModifier) {

            $modifier = ($operatorPriceModifier / 100);

            $newTotal = $oldTotal * $modifier;
            //put the template var only if necesarry
            $templateVars['newTotal'] = $newTotal;
        }
        
        //passing only defined vars makes code more flexible
        
        return $this->render('ExampleOneBundle:Operator:index.html.twig', $templateVars);
    }

    //made two functions into one because there is no point to keep them both
    private function getOperatoroPriceModifier() {
        if (isset($_SESSION['customer']) && $_SESSION['customer'] instanceof Operator) {
            return $this->container->getParameter('example_one.operator_price_modifier');
        }
    }

    /**
     * ok, this method is necessary only for this example - we have to somehow add items to cart :)
     * but you can still refactorize it
     */
    /* translete entity getters and setters into correct programming language
     * function returns init 
     */

    private function getCartItem1() {
        $item = new Item();
        $item->setName("TV LCD");
        $item->setPrice(100);
        return $item;
    }

    private function getCartItem2() {
        $item = new Item();
        $item->setName("Samsung S1000");
        $item->setPrice(200);
        return $item;
    }

}
