<?php

namespace ExampleOneBundle\Entity;

use ExampleOneBundle\Model\ItemInterface;

class Item implements ItemInterface {

    //translated properties and methods into right programming language

    protected $price;
    
    protected $name;

    function setPrice($price) {
        $this->price = $price;
    }

    function setName($name) {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

}
